#include<iostream>
#include<limits.h>
using namespace std;
int main(){
	char chMin = SCHAR_MIN;
	char chMax = SCHAR_MAX;
	unsigned char unChMax = UCHAR_MAX;
	short shortMin = SHRT_MIN;
	short shortMax = SHRT_MAX;
	unsigned short unShortMax = USHRT_MAX;
	int intVarMin = INT_MIN;
	int intVarMax = INT_MAX;
	unsigned int unIntMax = UINT_MAX;
	long longMin = LONG_MIN;
	long longMax = LONG_MAX;
	unsigned long unLongMax = ULONG_MAX;
	long long longlongMax = LLONG_MIN;
	long long longlongMin = LLONG_MAX;
	unsigned long long unLongLongMax = ULLONG_MAX;
	cout << "Char: " << static_cast<int>(chMin) << " : " << static_cast<int>(chMax) << endl;
	cout << "Unsigned Char maximum: " << static_cast<int>(unChMax) << endl;
	cout << "Short: " << static_cast<int>(shortMin) << " : " << static_cast<int>(shortMax) << endl;
	cout << "Unsigned Short maximum: " << static_cast<int>(unShortMax) << endl;
	cout << "Int: " << intVarMin << " : " << intVarMax << endl;
	cout << "Unsigned Int maximum: " << unIntMax << endl;
	cout << "Long: " << longMin << " : " << longMax << endl;
	cout << "Unsigned long maximum: " << unLongMax << endl;
	cout << "Long Long: " << longlongMin << " : " << longlongMax << endl;
	cout << "Unsigned long long maximum: " << unLongLongMax << endl;
	return 0;
}
