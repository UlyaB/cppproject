#include<iostream>
using namespace std;
int main(){
	char alph = 'a';
	int count = 1;
	while (alph <= 'z'){
		cout << "#" << count << ": " << alph << "-" << static_cast<int>(alph) << " ";
		++alph;
		if (count % 3 == 0){
			cout << "\n" << endl;
		}
		++count;
	}
	cout << "\n";
	return 0;
}
