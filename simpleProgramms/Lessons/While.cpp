//V 1.0
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
#include<iostream>
using namespace std;
int main(){
	int a = 97;
	while(a <= 122){
		cout << static_cast<char>(a) << " ";
		++a;
	}
	cout << "\n";

	/*
	char b = 'a';
	while(b <= 'z'){
		cout << b << " ";
		++b;
	}
	cout << "\n";
	*/
	return 0; 
}
//Output
/*

*/
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
//END FILE
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
