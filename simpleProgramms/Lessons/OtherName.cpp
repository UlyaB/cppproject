//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
//Простые примеры логических выражений
// V 1.0
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
#include<iostream>
using namespace std;
int main(){
	cout << boolalpha;
	/*
	bool a = false;
	a = !a;
	bool aa = true, bb = true;
	cout << a << endl;
	cout << (aa == bb) << endl;
	cout << (aa && bb) << endl;
	cout << (aa || bb) << endl;
	cout << (aa != bb) << endl;
	*/
	bool a, b, res;
	int choose;
	cout << "Введите значение переменных: ";
	cin >> a, cin >> b;
	cout << "Выбрать логическую опрецию (1 - &&, 2 - ||, 3 - ^, 4 - ==, 5 - !=, 6 - ): ";
	cin >> choose;
	switch (choose){
		case 1:
			res = (a && b); // И
			break;
		case 2:
			res = (a || b); // ИЛИ
			break;
		case 3:
			res = (a ^ b); // ИСКЛЮЧАЮЩАЯ ИЛИ
			break;
		case 4:
			res = (a == b); // РАВНО
			break;
		case 5:
			res = (a != b); // НЕ
			break;
		default:
			cout << "Try again!";
			return main();
	}
	cout << res << endl;
	return 0;
}
//Output
/*
true
*/
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
//END FILE
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
