#include<iostream>
#include<stdlib.h>
using namespace std;
int main(){
	char ch = getRandomNumber(97, 122);
	int count = 0;
	bool exitLoop = false;// Флаг для завершения выполнения цикла
	while (!exitLoop){
		cout << "Введите символ: ";
		char choose;
		cout << ch << endl;
		cin >> choose;
		if(ch == choose){
			exitLoop = true;// Маняем значение флага, чтобы выйти из цикла
		}
		else{
			++count;
			cout << "Количество попыток: " << count << endl;
			if(count == 10){
				cout << "Вы проиграли\nСимвол: " << ch << endl;
				exitLoop = true;
			}
		}
	}
	cout << "Попытки: " << count << "\n";
	return 0;
}
