//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
//Булевый тип данных. Простые примеры
//V 1.0
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
#include <iostream>
using namespace std;
int main(){
	cout << boolalpha;
	bool a = false, b = true;
	cout << a << "\t "<< b << endl;
	cout << "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-\n";
	int aa = 5, bb = 12;
	bool r = (aa == bb) ? true : false;
	cout <<"Aa равно bb? " << r << endl;
	r = (aa < bb) ? true : false;
	cout <<"Aa меньше bb? " << r << endl;
	r = (aa > bb) ? true : false;
	cout <<"Aa больше bb? " << r << endl;
	r = (aa <= bb) ? true : false;
	cout <<"Aa меньше или равно bb? " << r << endl;
	r = (aa <= bb) ? true : false;
	cout <<"Aa больше или равно bb? " << r << endl;
	r = (aa != bb) ? true : false;
	cout <<"Aa не равно bb? " << r << "\a" <<  endl;
	r = (aa < bb) || ( aa > bb );
	cout << "Сравнение ИЛИ " << r << endl;
	r = (aa < bb) && ( aa > bb );
	cout << "Сравнение И " << r << endl;
	r = (aa != bb) xor ( aa < bb );
	cout << "Сравнение исключающее ИЛИ XOR " << r << endl;
	cout << "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-\n";	
	return 0;
	}
//Output
/*
false	 true
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
Aa равно bb? false
Aa меньше bb? true
Aa больше bb? false
Aa меньше или равно bb? true
Aa больше или равно bb? true
Aa не равно bb? true
Сравнение ИЛИ true
Сравнение И false
Сравнение исключающее ИЛИ XOR false
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
*/
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
//END FILE
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
