//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
//Вычисление суммы натуральных чисел
// V 1.0
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
#include <iostream>
#include<unistd.h>
using namespace std;
int main(){
	cout << boolalpha;
	char abc[] = {"Alex"};
	char bcd[] = {"Programmer"};
	cout << abc << "\t" << bcd << endl;
	cout << (sizeof(bcd) / sizeof(bcd[0])) << endl;
	cout << "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-\n";
	string st = "ULIANA";
	string str = " PROGRAMMER";
	cout << st + str << endl;
	cout << "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-\n";
	int a = 5, b = 6, res = a + (b - 3);
	cout << res << endl;
	cout << "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-\n";
	int aa = 5, bb = 6;
	int result = aa * bb;
	cout << result << endl;
	int aaa = 12, bbb = 2, rrr = aaa/bbb;
	cout << rrr << endl;
	int rrrr = (6+4)-9*1+9;
	cout << rrrr << endl;
	cout << "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-\n";
	int aaaa = 5, bbbb = 6;
	cout << (aaaa += bbbb) << endl;
	cout << (aaaa -= bbbb) << endl;
	cout << (aaaa *= bbbb) << endl;
	cout << (aaaa /= bbbb) << endl;
	cout << ((20 % 3) ? false : true) << endl;	
	cout << "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-\n";	
	sleep(1);
	cout << "Сигнал с задержкой" <<"\7" << endl;
	return 0;
}
//Output
/*
Alex	Programmer
11
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
ULIANA PROGRAMMER
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
8
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
30
6
10
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
11
5
30
5
false
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
Сигнал с задержкой
*/
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// END FILE
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
