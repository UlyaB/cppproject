//Операторы if else	
// V 1.0
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
#include <iostream>
using namespace std;
int main(){
	int x = 0;
	cout << "Введите натуральное число: ";
	cin >> x;
	if ( x > 15){
		cout << "> 15\n";
	}
	else{
		cout << "< 15\n";
	}
	return 0;
}
//Output
/*

*/
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
//END FILE
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
