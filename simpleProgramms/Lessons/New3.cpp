//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
//Знаковые и беззнаковые 
//V 1.0
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
#include<iostream>
#include<limits.h>
using namespace std;
int main(){
	int a = 255;
	unsigned int b = -255;
	cout << -a << "\t" <<  b << endl;
	cout << sizeof(a) << "\t" << sizeof(b) << "\n";
	cout << "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-\n";
	int an = INT_MIN, ax = INT_MAX;
	unsigned int c = 0, cx = UINT_MAX; 
	cout << an << "\t" << ax << "\n";
	cout << c << "\t" << cx << "\n";
	return 0;
}
//Output
/*

*/
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
//END FILE
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
